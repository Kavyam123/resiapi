package com.student.test;


import org.apache.logging.log4j.*;

//import static restassured.RestAssured.*;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;


public class TC01_GETUserId {
	
	
	public static String slogpath = System.getProperty("/Users/kavyamanchiraju/Documents/Phani/work/dataStructures/student.test/properties/log4j2.properties");
	public static Logger logger = LogManager.getLogger(TC01_GETUserId.class);
	
	@Test 
	public void getUserId(){
	String hostname = "http://jsonplaceholder.typicode.com";
	String uri = "/posts";
	String url = hostname+uri;
	
	
	RestAssured.baseURI = url;
	
	RequestSpecification httprequest = RestAssured.given();
	
	Response response = httprequest.request(Method.GET,"/7");
	
	logger.debug("The response is displayed : " + response);
	
	String useridResponse = response.getBody().asString();
	
	logger.debug("UserID is found : " + useridResponse);
	
	System.out.println("User id from the responsebody is : " + useridResponse);
	
	String id = response.jsonPath().getString("id");
	
	Assert.assertEquals(id, "7");
	
	String title = response.jsonPath().getString("title");
	
	System.out.println("The title of the userID is : " + title);
	
	Assert.assertEquals(title, "magnam facilis autem");
	
	logger.debug("The title got displayed");
	}
	
}
