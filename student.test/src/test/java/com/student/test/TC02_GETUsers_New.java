package com.student.test;



import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import junit.framework.Assert;

   
public class TC02_GETUsers_New extends TC02_Stubbing_Test {

	static Logger logger = LogManager.getLogger(TC02_Server_Setup.class);
	
	

	@Test(priority = 1)
	public static void getusername() {
		
		String hostname = "http://localhost:8088";
		String uri = "/getUsers";
		String url = hostname+uri;
		
		RestAssured.baseURI = url;
		Response response1 = RestAssured.given().contentType("application/json").get();
		
		System.out.println("The response is : " + response1.asString());
		String responseBody = response1.getBody().asString();
		System.out.println("The response is : " + responseBody);
		logger.info("The response is displayed");
		
		System.out.println("The status code is : " + response1.statusCode());
		assertThat(response1.statusCode(), equalTo(200));
		logger.info("The status code is displayed");
		
		String userName = response1.jsonPath().getString("username");
		System.out.println("The user name is : " + userName);
		Assert.assertEquals(userName, "aa@m.com");
		logger.info("The username from the list is displayed");
	}
	
	@Test(priority = 2)
	public static void getSessionId() {
		
		String hostname = "http://localhost:8088";
		String uri = "/getUsers";
		String url = hostname + uri;
		
		RestAssured.baseURI = url;
		Response response2 = RestAssured.given().contentType("application/json").get();
		
		System.out.println("The response 2 is : " + response2.asString());
		logger.info("The response2 is displayed");
		System.out.println("The status code of response2  is : " + response2.statusCode());
		logger.info("The status code is displayed");
		Assert.assertEquals(response2.statusCode(), 200);
		
		JsonPath result  = from(response2.asString());
		
		ArrayList<Integer> sessionId = result.get("sessionid");
		System.out.println("The list of session id is :" + sessionId);
		logger.info("The list of session id is :" + sessionId);
		
		for(int i=0;i<sessionId.size();i++) {
			
			System.out.println("The session id is : " + sessionId.get(i));
		}
		
		int lastSessionId = sessionId.get(sessionId.size()-1);
		System.out.println("The last value of sesionid is : " + lastSessionId);
		logger.info("The last value of sesionid is : " + lastSessionId);
		Assert.assertEquals(lastSessionId, 56);
	}
		
	@Test(priority = 3)
	public static void getMarks() {
		
		String hostname = "http://localhost:8088";
		String uri = "/getUsers";
		String url = hostname + uri;
		RestAssured.baseURI = url;
		
		Response response3 = RestAssured.given().contentType("application/json").get();
		
		System.out.println("The response3 is: " + response3.asString());
		logger.info("The response3 is displayed" + response3.asString());
		System.out.println("The status code of response3 is " + response3.statusCode());
		logger.info("The status code of response3 is displayed" +response3.statusCode());
		Assert.assertEquals(response3.statusCode(), 200);
		
		JsonPath result1 = from(response3.asString());
		
		List<HashMap<String,Integer>> students = result1.getList("students");
		List<List<Integer>> marks = result1.getList("students.marks");
		System.out.println("the students list is " + marks);
		System.out.println("The marks lists from the students :" + marks.get(1));
	
		logger.info("The marks lists from the students :" + marks.get(1));
		
		ArrayList<Integer> marksList = new ArrayList<Integer>();
		marksList.add(0, 20);
		marksList.add(1, 25);
		marksList.add(2, 22);
		
		Assert.assertEquals(marks.get(1), marksList);
		
	}
		
		
	@Test(priority = 4)
	public static void getStateValue() {
		
		String hostname = "http://localhost:8088";
		String uri = "/getUsers";
		String url = hostname+uri;
		
		RestAssured.baseURI = url;
		Response response4 = RestAssured.given().contentType("application/json").get();
		
		System.out.println("The response of the getStateValue is :" + response4.asString());
		logger.info("The response4 is displayed");
		System.out.println("The status code of response4 is " + response4.statusCode());
		logger.info("The status code of response4 is displayed" +response4.statusCode());
		Assert.assertEquals(response4.statusCode(), 200);
		
		JsonPath result2 = from(response4.asString());
		
		List<HashMap<String,String>> studentlist = result2.getList("students");
		System.out.println(studentlist);
//		for (int i=0;i<studentlist.size();i++) {
//			HashMap<String, String>   studentDetails = studentlist.get(i);////HashMap<String, String>
//			System.out.println(studentDetails);
//		}
			List<ArrayList<?>> address =  result2.getList("students.adresss");
			HashMap<String,String> city = (HashMap<String,String>)address.get(1).get(1);
			System.out.println("The second state of a  student is " + city);
			System.out.println("The second state of a  student is " + city.get("state"));
			Assert.assertEquals(city.get("state"), "ca");
			}

	}
		
		
		
		
		
		
		
		
		
		
		
		
	

