package com.student.test;


import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;


import org.testng.annotations.BeforeTest;


public class TC02_Stubbing_Test extends TC02_Server_Setup {

	@BeforeTest
	public static void getMapping() {
		server.stubFor(get(urlEqualTo("/getUsers"))
		.withHeader("Content-Type",equalTo("application/json; charset=UTF-8"))
		.willReturn(aResponse()
		.withStatus(200)
		.withHeader("Content-Type","application/json; charset=UTF-8")
//		.withBody("{\n" + 
//				"  \"username\": \"aa@m.com\",\n" + 
//				"  \"password\": \"12xyz\",\n" + 
//				"  \"sessionid\": [\n" + 
//				"    12,\n" + 
//				"    23,\n" + 
//				"    34,\n" + 
//				"    56\n" + 
//				"  ],\n" + 
//				"  \"students\": [\n" + 
//				"    {\n" + 
//				"      \"id\": 112,\n" + 
//				"      \"name\": \"aana\",\n" + 
//				"      \"marks\": [\n" + 
//				"        20,\n" + 
//				"        25,\n" + 
//				"        22\n" + 
//				"      ],\n" + 
//				"      \"contact\": [\n" + 
//				"        \"1234\",\n" + 
//				"        \"3456\"\n" + 
//				"      ],\n" + 
//				"      \"adresss\": [\n" + 
//				"        {\n" + 
//				"          \"state\": \"nc\",\n" + 
//				"          \"city\": \"abc\"\n" + 
//				"        },\n" + 
//				"        {\n" + 
//				"          \"state\": \"ca\",\n" + 
//				"          \"city\": \"xyz\"\n" + 
//				"        }\n" + 
//				"      ]\n" + 
//				"    },\n" + 
//				"    {\n" + 
//				"      \"id\": 115,\n" + 
//				"      \"name\": \"banu\",\n" + 
//				"      \"marks\": [\n" + 
//				"        20,\n" + 
//				"        25,\n" + 
//				"        22\n" + 
//				"      ],\n" + 
//				"      \"contact\": [\n" + 
//				"        \"4534\",\n" + 
//				"        \"3456\"\n" + 
//				"      ],\n" + 
//				"      \"adresss\": [\n" + 
//				"        {\n" + 
//				"          \"state\": \"nc\",\n" + 
//				"          \"city\": \"abc\"\n" + 
//				"        },\n" + 
//				"        {\n" + 
//				"          \"state\": \"ca\",\n" + 
//				"          \"city\": \"xyz\"\n" + 
//				"        }\n" + 
//				"      ]\n" + 
//				"    }\n" + 
//				"  ]\n" + 
//				"}")));
		.withBodyFile("Users.json")));
		
			
				
	}
	
}
