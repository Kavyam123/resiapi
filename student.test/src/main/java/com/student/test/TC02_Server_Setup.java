package com.student.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.github.tomakehurst.wiremock.WireMockServer;

public class TC02_Server_Setup {

	public static int PORT = 8088;
	public static WireMockServer server;
	public static String slogpath = System.getProperty("/Users/kavyamanchiraju/Documents/Phani/work/dataStructures/student.test/properties/log4j2.properties");
	public static Logger logger = LogManager.getLogger(TC02_Server_Setup.class);
	
	
	
	@BeforeSuite
	public static void setupu() {
		server = new WireMockServer(PORT);
		slogpath = System.getProperty("/Users/kavyamanchiraju/Documents/Phani/work/dataStructures/student.test/properties/log4j2.properties");
		server.start();
		
	}
	
	@AfterSuite
	public static void stopserver() {
		server.stop();
	}
}
